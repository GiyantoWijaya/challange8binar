import React from 'react'
import './App.css';
import Home from './components/pages/index';
import Layout from './components/layout/index';
import { BrowserRouter, Route, Routes } from 'react-router-dom';


function App() {

  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route exact path="/" element={<Home />} />
        </Routes>
      </Layout>
    </BrowserRouter >
  );
}

export default App;
