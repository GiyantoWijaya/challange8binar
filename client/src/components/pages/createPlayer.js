import React from 'react';
import BtnModal from '../common/btnModal';
import { useDisclosure } from '@chakra-ui/react';


const CreatePlayer = ({ formData, setFormData }) => {
  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [experience, setExperience] = React.useState('');
  const [level, setLevel] = React.useState('');
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = React.useRef(null);

  const handleSubmit = (event) => {
    event.preventDefault();
    const newFormData = [name, email, password, experience, level];

    const isDuplicate = formData.some((data) => JSON.stringify(data) === JSON.stringify(newFormData));
    if (!isDuplicate) {
      setFormData([...formData, newFormData]);
      setName('');
      setEmail('');
      setPassword('');
      setExperience('');
      setLevel('');
      onClose();
    }
  }

  return (
    <BtnModal
      buttonName={'Create Player'} btnColor={'blue'}
      modalTitle={'Create A New Player'}
      input1={'Name'} value1={name} result1={(e) => setName(e.target.value)}
      input2={'Email'} type2={'email'} value2={email} result2={(e) => setEmail(e.target.value)}
      input3={'Password'} type3={'password'} value3={password} result3={(e) => setPassword(e.target.value)}
      input4={'Experience'} type4={'number'} value4={experience} result4={(e) => setExperience(e.target.value)}
      input5={'Level'} type5={'number'} value5={level} result5={(e) => setLevel(e.target.value)}
      handleSubmit={handleSubmit}
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      initialRef={initialRef}
    />

  );
}

export default CreatePlayer;