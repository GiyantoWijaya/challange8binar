import React from 'react';
import BtnModal from '../common/btnModal';
import { useDisclosure } from '@chakra-ui/react';



const UpdatePlayer = ({ id, value, formData, setFormData }) => {
  const [name, setName] = React.useState(value[0]);
  const [email, setEmail] = React.useState(value[1]);
  const [password, setPassword] = React.useState(value[2]);
  const [experience, setExperience] = React.useState(value[3]);
  const [level, setLevel] = React.useState(value[4]);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = React.useRef(null);

  const handleSubmit = (event) => {

    event.preventDefault();
    const updatedFormData = [...formData];
    updatedFormData[id] = [name, email, password, experience, level];
    setFormData(updatedFormData);
    onClose();
  }

  return (
    <BtnModal
      buttonName={'Update Player'} btnColor={'teal'}
      modalTitle={'Update an Existing Player'}
      input1={'Name'} value1={name} result1={(e) => setName(e.target.value)}
      input2={'Email'} type2={'email'} value2={email} result2={(e) => setEmail(e.target.value)}
      input3={'Password'} type3={''} value3={password} result3={(e) => setPassword(e.target.value)}
      input4={'Experience'} type4={'number'} value4={experience} result4={(e) => setExperience(e.target.value)}
      input5={'Level'} type5={'number'} value5={level} result5={(e) => setLevel(e.target.value)}
      handleSubmit={handleSubmit}
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      initialRef={initialRef}
    />

  );
}

export default UpdatePlayer;