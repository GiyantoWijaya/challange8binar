import React from 'react'
import BtnSearch from '../common/btnSearch';

const SearchPlayer = ({ onSearch }) => {
  const [searchValue, setSearchValue] = React.useState('');
  const [category, setCategory] = React.useState('');

  const handleSearchBtn = (event) => {
    event.preventDefault();
    if (document.getElementById('username').checked) {
      setCategory('username');
    } else if (document.getElementById('email').checked) {
      setCategory('email');
    } else if (document.getElementById('experience').checked) {
      setCategory('experience');
    } else if (document.getElementById('level').checked) {
      setCategory('level');
    }
    onSearch(searchValue, category);
  }

  const handleCategoryChange = (event) => {
    setCategory(event.target.value);
  }

  return (
    <BtnSearch
      search={(e) => setSearchValue(e.target.value)}
      isChecked1={handleCategoryChange}
      isChecked2={handleCategoryChange}
      isChecked3={handleCategoryChange}
      isChecked4={handleCategoryChange}
      result={handleSearchBtn}
    />
  );
}

export default SearchPlayer