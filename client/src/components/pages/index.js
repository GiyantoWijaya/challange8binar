import React from 'react'
import CreatePlayer from './createPlayer';
import UpdatePlayer from './updatePlayer';
import DataTable from '../common/dataTable';
import SearchPlayer from './searchPlayer';

const Home = () => {
  const [formData, setFormData] = React.useState([]);
  const [searchValue, setSearchValue] = React.useState('');
  const [category, setCategory] = React.useState('');

  return (
    <div className="container">
      <h1 className='text-center mt-5 mb-5'>Home</h1>
      <SearchPlayer onSearch={(searchValue, category) => {
        setSearchValue(searchValue);
        setCategory(category);
      }} />
      {searchValue.length > 0 && (<h5 className='text-center mt-3 mb-3'>The result of search is <h3>{searchValue}</h3> from Category : <h3>{category}</h3></h5>)}
      <CreatePlayer formData={formData} setFormData={setFormData} />
      {formData.length > 0 && (
        <DataTable
          data={formData}
          action={(index, value) => (
            <UpdatePlayer
              id={index}
              value={value}
              formData={formData}
              setFormData={setFormData}
            />
          )}
        />
      )}
    </div>
  );
};

export default Home;