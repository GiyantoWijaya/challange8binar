import React from 'react';

const BtnSearch = (props) => {
  return (
    <div className='container-fluid mb-3'>
      <form onSubmit={props.result}>
        <div className='text-center'>
          <div className="form-check form-check-inline">
            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="username" value='username' onChange={props.isChecked1} />
            <label className="form-check-label">Username</label>
          </div>
          <div className="form-check form-check-inline">
            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="email" value='email' onChange={props.isChecked2} />
            <label className="form-check-label">Email</label>
          </div>
          <div className="form-check form-check-inline">
            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="experience" value='experience' onChange={props.isChecked3} />
            <label className="form-check-label" >Experience</label>
          </div>
          <div className="form-check form-check-inline">
            <input className="form-check-input" type="radio" name="inlineRadioOptions" id="level" value='level' onChange={props.isChecked4} />
            <label className="form-check-label">Level</label>
          </div>
        </div>
        <div className='d-flex'>
          <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" onChange={props.search} />
          <button className="btn btn-outline-success" type="submit">Search</button>
        </div>
      </form>
    </div>
  );
}

export default BtnSearch;