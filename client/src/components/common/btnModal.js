import React from 'react'
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Button,
  FormControl,
  FormLabel,
  Input
} from '@chakra-ui/react';


const BtnModal = (props) => {

  return (
    <>
      <Button onClick={props.onOpen} colorScheme={props.btnColor}>{props.buttonName}</Button>

      <Modal
        isOpen={props.isOpen}
        onClose={props.onClose}
      >
        <ModalOverlay />
        <ModalContent>
          <form onSubmit={props.handleSubmit} >
            <ModalHeader>{props.modalTitle}</ModalHeader>
            <ModalCloseButton />
            <ModalBody pb={6}>
              <FormControl>
                <FormLabel>{props.input1}</FormLabel>
                <Input value={props.value1} type={props.type1} ref={props.initialRef} placeholder={props.input1} onChange={props.result1} />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>{props.input2}</FormLabel>
                <Input value={props.value2} type={props.type2} placeholder={props.input2} onChange={props.result2} />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>{props.input3}</FormLabel>
                <Input value={props.value3} type={props.type3} placeholder={props.input3} onChange={props.result3} />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>{props.input4}</FormLabel>
                <Input value={props.value4} type={props.type4} placeholder={props.input4} onChange={props.result4} />
              </FormControl>

              <FormControl mt={4}>
                <FormLabel>{props.input5}</FormLabel>
                <Input value={props.value5} type={props.type5} placeholder={props.input5} onChange={props.result5} />
              </FormControl>

            </ModalBody>

            <ModalFooter>
              <Button colorScheme='green' mr={3} type="submit">
                Submit
              </Button>
              <Button colorScheme='red' onClick={props.onClose}>Cancel</Button>
            </ModalFooter>
          </form >
        </ModalContent>
      </Modal >
    </>
  );

}

export default BtnModal;