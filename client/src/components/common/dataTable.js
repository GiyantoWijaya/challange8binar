import React from 'react'
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from '@chakra-ui/react'

const DataTable = ({ data, action }) => {
  return (
    <TableContainer>
      <Table variant='simple'>
        <TableCaption><h3>Data Players</h3></TableCaption>
        <Thead>
          <Tr>
            <Th>Id</Th>
            <Th>Name</Th>
            <Th>Email</Th>
            <Th>Password</Th>
            <Th isNumeric>Experience</Th>
            <Th isNumeric>Level</Th>
            <Th>Actions</Th>
          </Tr>
        </Thead>
        <Tbody>
          {data.map((dataRow, index) => (
            <Tr key={index}>
              <Td>{index + 1}</Td>
              <Td>{dataRow[0]}</Td>
              <Td>{dataRow[1]}</Td>
              <Td>{dataRow[2]}</Td>
              <Td isNumeric>{dataRow[3]}</Td>
              <Td isNumeric>{dataRow[4]}</Td>
              <Td>{action(index, dataRow)}</Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </TableContainer>
  );

}

export default DataTable;