import React from 'react'
import { NavLink } from 'react-router-dom';

const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-lg bg-secondary-subtle">
      <div className="container-fluid">
        <NavLink className="navbar-brand mx-3"><h1>Challange-8</h1></NavLink>
        <span><p>Tombol Update akan muncul ketika Create player</p></span>
      </div>
    </nav>
  )
}

export default Navbar;